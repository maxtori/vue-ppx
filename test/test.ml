open Js_of_ocaml.Js

let log x = Js_of_ocaml.Firebug.console##log x

module CTest0 = struct
  {%%template|
  <div>bla</div>
  |}
  [%%comp {name="c0"}]
end

module CTest1 = struct

  {%%render|
  <div>
    {{ pP }}
    <button @click="ping()">ping</button>
    <span ref="truc"></span>
    <c0></c0>
  </div>
  |}

  let%data bla : string list = []
  and bla2 = 0

  let%prop pP : string = ""
  let%watch pP _this new_ old =
    Format.printf "new: %s, old: %s" new_ old

  [%%mounted fun _this -> Format.printf "component mounted"]
  let%meth ping _this (x: string) : unit =
    Format.printf "%s" x

  [%%comp {components=[CTest0]; types; conv}]
end

let%data message = "hello"
let%comp bla this : string = to_string this##.message ^ " ajsbf"
let%meth test this (x: string) : string =
  Format.sprintf "%s %s" (to_string this##.bla) x
and acknowledge_ping _this x =
  Format.printf "ping acknowledged";
  log x;
  log _this##.message

let%global global_msg1 = "glob message 1"

[%%app { components=[CTest1.component]; mount; export; unhide; convert; debug } ]
