all: build
build:
	@dune build src
clean:
	@dune clean
dev:
	@dune build --profile release
	@cp -f _build/default/test/test.bc.js test/test.js

compiler:
	@npm i --prefix src --no-audit --no-fund
	@src/node_modules/@vercel/ncc/dist/ncc/cli.js build src/render.js -m -q -o src
	@mv src/index.js src/render.bundle.js
